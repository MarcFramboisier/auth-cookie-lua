require 'apache2'
-- variable d'environnement doivent etre definie pour que le module fonctionne
-- AUTH_SECRET : le secret pour l'authentification
-- AUTH_NOM_COOKIE : le nom du cookie
-- AUTH_URL_RETOUR_AUTH : ou renvoyer l'utilisateur apres l'authentification
-- AUTH_DUREE_VALIDE : duree avant peromption du cookie
-- AUTH_DOMAIN : le domaine du cookie
-- AUTH_URL_AUTH : l'url pour obtenir un cookie

function calculHash( r, secret , date )
	-- un genre de HMAC
	local res = r:sha1(secret  .. date)
	return res
end

function testVarEnv(r)
	-- verifie que toute les variable d'environnement necessaire a l'execution sont definies
	-- renvoie false si ce n'est pas le cas
	local listeVar = { "AUTH_SECRET" , "AUTH_NOM_COOKIE" , "AUTH_URL_RETOUR_AUTH" , "AUTH_DUREE_VALIDE" , "AUTH_DOMAIN" , "AUTH_URL_AUTH" }
	local res = true
	for key, varName in pairs(listeVar) do
		if r.subprocess_env[varName] == nil then
			res = false
			r:crit("auth.lue : environment variable " .. varName .. " must be defined")
		end
	end

	return res
end



function creerCookie(r)
	r:notice("creerCookie : entree")
	-- si les variable ne sont pas d�fini on sort par une erreur 500
	if testVarEnv(r) == false then
		return 500
	end
	-- la date d'expiration est la date courante + la duree de validite du cookie
	local expiration = os.time()  + r.subprocess_env['AUTH_DUREE_VALIDE']
	cookieVal= expiration .. ":" .. calculHash(r, r.subprocess_env['AUTH_SECRET'] , expiration ) 
	r:notice("creerCookie : le cookie est : " .. cookieVal)

	r:notice("creerCookie : date courante " .. os.time() )
	r.err_headers_out['Set-Cookie'] = r.subprocess_env['AUTH_NOM_COOKIE'].. "=" .. cookieVal .. ";Max-Age=" .. r.subprocess_env['AUTH_DUREE_VALIDE'] .. ";Domain=" .. r.subprocess_env['AUTH_DOMAIN'] .. ";Secure;HttpOnly;"
	-- ajout de l'entete location pour renvoyer l'utilisateur sur le site
	r.headers_out['Location'] = r.subprocess_env['AUTH_URL_RETOUR_AUTH']
	-- retour 302
	return apache2.HTTP_MOVED_TEMPORARILY
end 

function verifCookie(r)
	r:notice("verifCookie : entree : " .. r.uri)
	-- si les variable ne sont pas d�fini on sort par une erreur 500
	if testVarEnv(r) == false then
		return 500
	end
	if r.uri == r.subprocess_env['AUTH_URL_AUTH']then
		-- au cas ou nous sommes dans une requete de creation de Cookie
		r:notice("verifCookie : Url de creation du cookie : on sort")
		return apache2.DECLINED -- pour dire que l'on ne fait rien et laisser l'autre scirpt traiter la requette
	end
	local cookieVal = r:getcookie(r.subprocess_env['AUTH_NOM_COOKIE'])
	res = false -- signifie que l'on renvoie vers la page d'authentification
	if  cookieVal == nil then
		-- le cookie est absent
		res=false
	else
		-- on split le cookie
		local expiration, cle = cookieVal:match("([^:]+):([^:]+)")
		if (expiration == nil) or (cle==nil) then
			-- le cookie n'a pas le bon format
			res = false
			r:notice("verifCookie : mauvais format de Cookie")
		else
			r:notice("expiration : " .. expiration .. " et cle : " .. cle)
			if os.time() > tonumber(expiration) then
				-- cookie a expire
				r:notice("VerifCookie : cookie perime")
				res=false
			else
				recalculCle = calculHash(r , r.subprocess_env['AUTH_SECRET'] , expiration)
				-- on verifie que le cookie a ete genere avec la bonne cle
				if cle == recalculCle then
					res=true
				else
					res=false
					r:warn("VerifCookie : erreur verification cookie")
				end
			end
		end
	end
	if res then
		r:notice("verifCookie : OK")
		return apache2.OK
	else
		r:notice("verifCookie : KO")
		r.headers_out['Location'] = r.subprocess_env['AUTH_URL_AUTH']
		return apache2.HTTP_MOVED_TEMPORARILY
	end
end 

